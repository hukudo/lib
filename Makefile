MAKEFLAGS += --always-make

default: code-style unit-test

docs:
	make -C 1-docs/

clean:
	git clean -dfx -e .env -e .idea

code-style:
	ruff format .
	ruff check .

unit-test:
	docker-compose up -d  # grafana instance
	pytest --exitfirst --numprocesses=auto --doctest-modules --cov=. --cov-report=html --cov-report=term --cov-report=xml

build:
	python -m build

patch_release: clean unit-test
	bumpversion patch
	make build push_with_tags upload

minor_release: clean unit-test
	bumpversion minor
	make build push_with_tags upload

major_release: clean unit-test
	bumpversion major
	make build push_with_tags upload

push_with_tags:
	git push
	git push --tags

upload:
	twine upload --repository pypi-hukudo dist/*

requirements.txt:
	uv pip compile pyproject.toml --extra=all -o requirements.txt

dev-setup:
	pyenv virtualenv 3.10.11 hukudo
	pyenv local hukudo
	pip install -U uv
	uv pip install -e .[all]

e2e-test:
	./bin/e2e_test.sh
