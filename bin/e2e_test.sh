#!/bin/bash
set -euo pipefail


venv=$(mktemp -d)
python3.9 -m venv $venv/

finally() {
  rm -rf $venv
}
trap finally EXIT

shopt -s expand_aliases
alias pip=$venv/bin/pip
alias hukudo=$venv/bin/hukudo


pip install -U pip
pip install hukudo
hukudo --help
