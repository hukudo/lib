from io import BytesIO

import pytest
from openpyxl import Workbook

from hukudo.xls.sheet import Sheet
from hukudo.xls.table import Table

pytest_plugins = ['pytester']


@pytest.fixture
def wb():
    wb = Workbook()
    yield wb
    # Must save for some errors to become visible. Note that you will see exceptions during teardown.
    with BytesIO() as f:
        wb.save(f)


@pytest.fixture
def ws(wb):
    yield wb.active


@pytest.fixture()
def sh(ws):
    return Sheet(ws)


@pytest.fixture
def table():
    return Table(
        header=['col1', 'col2', 'col3', 'id'],
        data=[
            ['first', 'row', 'data', 'id data'],
            ['second', 'row', 'data', 'id data'],
        ],
    )


@pytest.fixture
def table_single_row():
    return Table(
        header=['col1', 'col2', 'col3', 'id'],
        data=[
            ['single', 'row', 'data', 'id data'],
        ],
    )
