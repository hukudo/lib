X = 'hukudo'
project = X
html_title = X
author = X
language = 'en'
extensions = [
    'myst_parser',
    'sphinx.ext.imgmath',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
]
myst_update_mathjax = False

templates_path = ['_templates']
source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown',
}
exclude_patterns = [
    '_build',
    'Thumbs.db',
    'CHANGELOG.md',
    'README.md',
]

html_theme = 'pydata_sphinx_theme'
html_theme_options = {
    'collapse_navigation': True,
}
html_static_path = ['_static']
html_css_files = ['custom.css']
html_favicon = '_static/favicon.png'
html_show_sourcelink = False
html_last_updated_fmt = '%Y-%m-%d %H:%M'

today_fmt = '%Y-%m-%d'

# https://myst-parser.readthedocs.io/en/latest/using/syntax-optional.html
myst_enable_extensions = [
    'linkify',
]
