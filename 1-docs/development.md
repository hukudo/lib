# Development

## Getting Started
Create a virtualenv and install in dev mode. Build docs.
```
make dev-setup
make docs
```

## Generate `requirements.txt`
```
uv pip compile pyproject.toml --upgrade --extra=all
```
