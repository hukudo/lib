# hukudo.grafana

## Installation and Configuration
```
uv pip install -e .
cat <<'EOF' > .env
GRAFANA_URL=https://grafana.example.com/
GRAFANA_API_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
GRAFANA_CLIENT_ROOT_CA=/etc/certs/root_ca.crt
GRAFANA_CLIENT_CRT=/etc/certs/instance.crt
GRAFANA_CLIENT_KEY=/etc/certs/instance.key
EOF
```

Note that variables that start with `GRAFANA_CLIENT_` are optional. You can use
them to set a client certificate to authorize against the server in [zero
trust][zt] settings.

[zt]: https://en.wikipedia.org/wiki/Zero_trust_security_model


## Backup all Grafana Dashboards
```python
from pathlib import Path
from hukudo.grafana import Grafana
grafana = Grafana.from_basic_auth('https://grafana.dev.0-main.de', 'admin', 'test')

for board in grafana.dashboards():
    filename = Path(f'/tmp/grafana-dashboard-{board.id}')
    board.export(filename)
```


## Similar Projects
- https://pypi.org/project/grafana-api/ unmaintained
  superseded by https://github.com/panodata/grafana-client
- https://github.com/panodata/grafana-client seems weird
  - https://github.com/panodata/grafana-client/blob/main/grafana_client/elements/admin.py#L14-L16
    this pattern repeats
  - no docs


## Explanation

### Finding all Dashboards
Folder API looks promising, but as the general folder [is special][general_0]
(has UID 0), the folder API does not allow to get all dashboards.
[general_0]: https://grafana.com/docs/grafana/latest/http_api/folder/#a-note-about-the-general-folder


## Known Issues
### `grafana.dashboards()` ignores paging
😅
