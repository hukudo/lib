# Release Management

## Configuring Twine
```
cat <<'EOF' > ~/.pypirc
[distutils]
index-servers=
    pypi-hukudo

[pypi-hukudo]
repository: https://upload.pypi.org/legacy/
username: __token__
password: pypi-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
EOF
```

## How to Release a New Version
```
make patch_release
```
